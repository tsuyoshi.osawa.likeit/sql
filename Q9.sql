SELECT 
	category_name,
	SUM(item.item_price) As total_price
From
	item 
INNER JOIN
	item_category
ON
	item.category_id = item_category.category_id
GROUP BY
	item.category_id
ORDER BY
	total_price DESC;


//total_priceはitemテーブルから別名として生まれたので、itemテーブルではない。
